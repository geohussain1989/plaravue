<?php

namespace App\Services;

use App\Models\Menu;

class MenuService
{
    public function getMenuWithCategories($restaurant_ids)
    {
        return $categories = Menu::whereIn('restaurant_id', $restaurant_ids)
            ->get()
            ->groupBy('category.name');
    }
}
