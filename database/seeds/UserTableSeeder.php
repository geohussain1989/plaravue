<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Syed Hussain',
            'email'    => 'geohussain1989@gmail.com',
            'password' => bcrypt('huss4321')
        ]);
    }
}
