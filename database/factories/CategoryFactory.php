<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'name'          => $faker->word,
        'description'   => $faker->paragraphs(1, true),
        'restaurant_id' => 1,
    ];
});
